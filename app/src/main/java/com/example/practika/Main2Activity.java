package com.example.practika;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

    public class Main2Activity extends AppCompatActivity {
      private  WebView webView;
        private class MyWebViewClient extends WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return true;
            }
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main2);
            ActionBar actionBar = getSupportActionBar();
            //actionBar.setSubtitle((Html.fromHtml("<font color=\"#999999\">" + getString(R.string.app_subname) + "</font>")))
            Intent myIntent = getIntent();
            String myValue = myIntent.getStringExtra("myLogin");
            actionBar.setTitle(getString(R.string.app_name) + "  |   ПОЛЬЗОВАТЕЛЬ: "+myValue);
            // включаем поддержку JavaScript
                webView = findViewById(R.id.webView);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.setWebViewClient(new MyWebViewClient());
                if ((myValue.equals("privet"))) {
                    webView.loadUrl("http://developer.alexanderklimov.ru/android");
                }
                if ((myValue.equals("sun"))) {
                    webView.loadUrl("https://uk.wikipedia.org/wiki/%D0%A1%D0%BE%D0%BD%D1%86%D0%B5");
                }
                if ((myValue.equals("ula"))) {
                    webView.loadUrl("https://youla.ru/");
                }

        }


    }


